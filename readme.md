# TrackCash - Reader
<div align="center">
  <img alt="TackCash" title="TackCash" src="https://sistema.trackcash.com.br/images/logo/logo_trackcash_vert.png" width="300px"/>
</div>

## Descrição
Este projeto tem como objetivo fazer a leitura de dados dos buckets S3 da AWS no ciclo de automação.

O formato desse projeto foi projetado para ser executado de maneira serveless no serviço Lambda da AWS.

## Requisitos 

**Para desenvolvimento**
- [Python 3.6 ou superior](https://www.python.org/downloads/)

**Para build e deploy**

- [NPM](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
- [Serverless](https://www.serverless.com/framework/docs/getting-started/)
- [Docker](https://docs.docker.com/engine/install/)

## Preparando o ambiente
Para a execução do projeto de maneira local deve ser executado os seguintes passos.

- **Criação de ambiente virtual**: 

    Nesse passo será necessário executar os seguinte comando no terminal (dentro da pasta do projeto):
 
    **Exemplo para Linux**

    Instalar o módulo de ambiente virutal (caso não exista)

    `pip install virtualenv`
    
    Checar instalação

    `virtualenv --version`
    
    Criar ambiente virtual com nome 'env'

    `virtualenv env`
    
    Ativar ambiente virtual 

    `source env/bin/activate`
    
    Instalar dependências do projeto

    `pip install -r requirements.txt`
    
    **Exemplo para Windows**

    Criar ambiente virtual com nome 'env'
    
    `python -m venv env`

    Ativar ambiente virtual  

    `./env/Scripts/activate`

    Instalar dependências do projeto  

    `pip install -r requirements.txt`

- **Variáveis de ambiente**:

    O projeto atualmente trabalha com as seguintes variáveis de ambiente:

    `TOKEN -> Token para integração de serviços TrackCash`

    Rode os comandos a seguir para setar as variáveis:

    **Exemplo para Linux**

    Setar valor da variável

    `TOKEN=amVzc2ljYS5kaWFzQHRyYWNrY2FzaC5jb20uYnI6amVqZTExMTI=`

	Validar a variável

	`set | grep TOKEN`

	Verificar se a variável foi criada

    `echo $TOKEN`

    **Exemplo para Windows**

    Setar valor da variável

    `set TOKEN=amVzc2ljYS5kaWFzQHRyYWNrY2FzaC5jb20uYnI6amVqZTExMTI=`

    Pode ser feito através de interface gráfica também.


## Build e Deploy
	
A build e o deploy da aplicação é feita através do framework "Serverless", consulte a seguinte documentação em caso de dúvida:
[Documentação Serverless](https://www.serverless.com/framework/docs/) 

A aplicação necessita da seguinte dependência "serverless-python-requirements", para instala-la basta rodar o comando:
`npm install --save serverless-python-requirements`

Para o deploy da aplicação é necessário rodar o seguinte comando:
`serverless deploy`

**Importante**: Para que esse procedimento seja executado você deve estar com suas variáveis do AWS setadas no Serverless, consulte a documentação citada.

## Checklist de desenvolvimento

Marcação de itens desenvolvidos e a serem desenvolvidos no sistema

 - Carrefour
	- [x] Pedidos
	- [x] Repasse
- Leroy
	- [x] Pedidos
	- [x] Repasse
- Cnova
	- [x] Pedidos
	- [x] Repasse cartão
	- [x] Repasse boleto
- Magazine
	- [x] Pedidos
	- [x] Repasse
- B2w
	- [x] Pedidos
	- [x] Repasse
- Alpe
	- [x] Vendas
	- [x] Financeiro
	
- Mercado Livre
	- [ ] Pedidos
	- [ ] Repasse

- Madeira Madeira
	- [x] Pedidos
	- [x] Repasse
	
- Netshoes
	- [x] Pedidos
	- [x] Repasse
	
- Dafiti
	- [x] Repasse
	
- Amazon
	- [x] Pedidos
	- [x] Repasse
	
- Zoom
	- [x] Repasse

- GPA
    - [x] Pedidos
    - [x] Repasse
    
- Ezcommerce
	- [ ] Pedidos
	- [ ] Repasse
