from entities.Core import Core

class DafitiBank(Core):
    def __init__(self,path,readeable):
        super().__init__(
            path,
            readeable,
            [
                self.setIdWithdrawal,
                self.setEmptyIdOrder,
                self.setIdCode,
                self.setReference
            ]
        )

    def setEmptyIdOrder(self,msg,dic):

        if msg['id_order'] == '':

            msg['id_order'] = msg['id_withdrawal']

            return {'type':"inplace",'content':msg}

    def setIdWithdrawal(self,msg,dic):

        dateArray = self.idArchieve.split('.')[0].split('-')
        dateArray.reverse()

        idWithdrawal = ''.join(dateArray)

        msg['id_withdrawal'] = idWithdrawal

        return {'type':"inplace",'content':msg}

    def setIdCode(self,msg,dic):

        idCodeCorrelation = {
            'Comissão':'8',
            'Frete':'7',
            'Preço Total dos Produtos':'7',
            'Crédito de Comissão':'8',
            'Frete de Pedidos Devolvidos':'10',
            'Preço do item':'10',
            "Frete Dafiti Envios":'9',
            "Return: Commission":'10',
            "Postagem Reversa":'7',
        }

        msg['id_code'] = idCodeCorrelation.get(dic.get('tipo_transacao').strip(),"21")
        if msg['id_code'] == '21':
            msg['description'] = dic.get('tipo_transacao')
        else:
            msg['description'] = self.trackcashRefferences.balance.get(msg['id_code'])

        return {'type':"inplace",'content':msg}

    def setReference(self,msg,dic):

        idReferenceCorrelation = {
            'IRRF':'1',
            'Comissão':'2',
            'Frete':'3',
            'Preço Total dos Produtos':'4',
            'Crédito de Comissão':'5',
            'Frete de Pedidos Devolvidos':'6',
            'Preço do item':'7'
        }

        msg['reference'] = idReferenceCorrelation.get(dic.get('tipo_transacao'))

        return {'type':"inplace",'content':msg}
