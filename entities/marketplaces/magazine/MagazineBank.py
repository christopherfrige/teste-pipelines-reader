from datetime import datetime

from entities.Core import Core

class MagazineBank(Core):

    def __init__(self, path, readeable):
        super().__init__(
            path, 
            readeable, 
            [
                self.setIndex,
                # self.set_id_code,
                self.setIdWithdrawal,
                self.set_reference,
                self.setSellValue,
                self.setWithdrawalIdOrder,
                self.adjustEvent,
                self.setInstallmentOnDescription,
                self.adjusteDevolutive,
                self.extractComissions,
                self.anticipation_value,
                self.setDescriptionId21
            ]
        )

        self.sell_options = {
            'account_balance': '7',
            'cartão de crédito': '7',
            'cartão de débito': '7',
            'boleto': '7',
            'depósito': '7', 
            'pix': '7',
            'transferência': '17',
            'evento': '21',
            'estorno': '10'
        }

        self.originCorrelation = {
            "Pagarme":'1',
            "Magalu Pagamentos":'2',
            "Novo Magalu Pagamentos":'3'
        }

        self.typesCorrelation = {
            "account_balance":"1",
            "cartão de crédito":"2",
            "cartão de débito":"3",
            "boleto":"4",
            "depósito":"5",
            "evento":"6",
            "transferência":"7",
            "estorno":"8",
            "pix": "9"
        }
        
        self.idWithdrawals = self._setIdWithdrawals()
        self.index = 0

        self._verify_df_data()

    def setIndex(self,msg,dic):
        self.index = dic.get('Unnamed: 0')

    def set_id_code(self, msg, dic):
        # Save structure from static
        # "'7' if dic.get('payment_method').lower() in ['account_balance','cartão de crédito','cartão de débito','boleto','depósito', 'pix'] else '21' if dic.get('payment_method').lower() == 'evento' else '17' if dic.get('payment_method').lower() == 'transferência' else '10'"
        
        payment_method = dic.get('payment_method').lower()
        
        msg['id_code'] = self.sell_options.get(payment_method, '21')
        
        return {"type": 'inplace', 'content': msg}

    def setSellValue(self,msg,dic):
        if msg['id_code'] == '7' and msg['total'] == '':
            msg['total'] = dic.get('net_installment_amount') * 100

        return {"type": 'inplace', 'content': msg}

    def extractIdWithdrawal(self,dic):
        orderDate = datetime.strptime(
            dic.get('Data da transação'),
            '%d/%m/%Y %H:%M'
        ).strftime('%Y%m%d%H%M%S')

        idWithdrawal = orderDate + self.originCorrelation.get(dic.get('Origem'), '')

        return idWithdrawal

    def _setIdWithdrawals(self):
        idWithdraawalRelation = {}

        ciclesRows = self.trackcashServices.df[self.trackcashServices.df["Método de pagamento"] == 'Transferência'].to_dict('records')

        for register in ciclesRows:
            idWithdraawalRelation.update({register.get('Unnamed: 0'):self.extractIdWithdrawal(register)})

        return idWithdraawalRelation

    def setIdWithdrawal(self,msg,dic):

        for key in self.idWithdrawals:

            if self.index <= key:
                msg['id_withdrawal'] = self.idWithdrawals[key]
                break

        return {"type": 'inplace', 'content': msg}

    def adjustEvent(self,msg,dic):
        if msg.get('id_code') == '21':
            msg['id_order'] = dic.get('transaction_id')
            return {"type": 'inplace', 'content': msg}

    def adjusteDevolutive(self,msg,dic):
        if dic.get('payment_method').lower() == 'estorno':
            msg['total'] = dic.get('net_installment_amount')*100
            return {"type": 'inplace', 'content': msg}

    def extractComissions(self, msg, dic):
        if msg.get('id_code') == '7':
            aux_msg = msg.copy()
            aux_msg['id_code'] = '8'
            aux_msg['description'] = f"{self.trackcashRefferences.balance.get('8')} - {msg.get('current_installment')}/{msg.get('installments')}"
            aux_msg['total'] =  -(dic.get('installment_commission') * 100) if dic.get('installment_commission') != '' else None
            return {"type": 'clone', 'content': aux_msg}

    def setWithdrawalIdOrder(self,msg,dic):
        if msg.get('id_code') == '17':
            msg['id_order'] = msg['id_withdrawal']
        return {"type":'inplace',"content":msg}

    def anticipation_value(self, msg, dic):
        if msg.get('id_code') == '17':
            aux_msg = msg.copy()
            aux_msg['id_code'] = '18'
            aux_msg['description'] = self.trackcashRefferences.balance.get('18')
            aux_msg['total'] = -(dic.get('anticipation_amount') * 100) if dic.get('anticipation_amount') != '' else None
            aux_msg['id_order'] = msg['id_withdrawal']
            return {"type": 'clone', 'content': aux_msg}

    def setInstallmentOnDescription(self,msg,dic):
        if msg['id_code'] not in ('21','17','18'):
            msg['description'] = f'{msg["description"]} - {msg.get("current_installment")}/{msg.get("installments")}'
            return {"type": 'inplace', 'content': msg}

    def set_reference(self,msg,dic):
        type_reference = self.typesCorrelation.get(
            dic.get('payment_method', '').lower(),
            ''
        )

        referenceDatePart = datetime.strptime(
            dic.get('transaction_date', ''),
            '%d/%m/%Y'
        ).strftime(
            '%Y%m%d'
        )
        
        reference = referenceDatePart + type_reference
        msg['reference'] = reference

        return {"type": 'inplace', 'content': msg}

    def setDescriptionId21(self, msg, dic):

        msg_copy = msg.copy()

        if msg['id_code'] == '21':
            msg_copy['description'] = dic.get('payment_method')
        
        return {"type": 'inplace', 'content': msg_copy}

    def _verify_df_data(self):
        if self.trackcashServices.df.empty:
            print('Arquivo sem dados.')
            self.empty = True
        
        else: 
            self.empty = False