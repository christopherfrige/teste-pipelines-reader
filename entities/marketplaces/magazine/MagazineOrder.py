from entities.Core import Core


class MagazineOrder(Core):

    def __init__(self, path, readeable):
        super().__init__(
            path, 
            readeable, 
            [
                self.verify_shipment_service,
                self.verify_invoices_data
            ]
        )

        self._verify_df_data()

    @staticmethod
    def verify_shipment_service(msg, dic):
        options = {
            'Magalu Entregas': 60,
            'MagaLog': 61,
            'Logbee': 62
        }
        
        msg['shipments'][0]['shipment_service'] = options.get(dic.get('courrier')) 

        return {"type": 'inplace', 'content': msg}

    def verify_invoices_data(self, msg, dic):
        invoices_id = (
            msg
            .get('invoices')[0]
            .get('invoice_id')
        )

        if not invoices_id:
            del msg['invoices']

        return {"type":"inplace","content":msg}

    def _verify_df_data(self):
        if self.trackcashServices.df.empty:
            print('Arquivo sem dados.')
            self.empty = True
        
        else: 
            self.empty = False
