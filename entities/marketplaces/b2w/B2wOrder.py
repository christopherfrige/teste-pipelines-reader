from entities.Core import Core

class B2wOrder(Core):

    def __init__(self,path, readeable):
        super().__init__(
            path, 
            readeable,
            [
                self.adjust_id_channel,
                self.verify_invoices_data
            ]
        )
        
        self._verify_data()

    def adjust_id_channel(self, msg, dic):

        prefix_order = dic.get('pedido_b2w').split('-')[0]

        if prefix_order == '01':
            msg['id_channel'] = '9'

        elif prefix_order == '03':
            msg['id_channel'] = '10'

        elif prefix_order == '02':
            msg['id_channel'] = '5'

        return {"type":"inplace","content":msg}

    def verify_invoices_data(self, msg, dic):
        invoices_id = (
            msg
            .get('invoices')[0]
            .get('invoice_id')
        )

        if not invoices_id:
            del msg['invoices']

        return {"type":"inplace","content":msg}

    def _verify_data(self):
        data = self.trackcashServices.df['mensagem'].tolist()[0]
        
        if (
            isinstance(data, str) and
            ('Não foram encontrado registros' in data)
        ):
            print('Arquivo sem dados.')
            self.empty = True
        
        else:
            self.empty = False
            
