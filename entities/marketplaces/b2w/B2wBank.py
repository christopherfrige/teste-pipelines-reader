from entities.Core import Core


class B2wBank(Core):

    def __init__(self, path_file, content_file):
        super().__init__(
            path_file, 
            content_file,
            [
                self.impeed_locked_register, 
                self.set_id_withdrawal, 
                self.set_reference, 
                self.set_id_code,
                self.set_id_channel
            ]
        )

        self._verify_data()

        self.id_channel_correlation = {
            'SHOP': '9', 
            'SUBA': '10', 
            'ACOM': '5', 
            'AEMP': '5'
        }

        self.id_code_correlation = {
            "estorno_venda": '10',
            "estorno_comissao": '12',
            "venda": '7',
            "comissao": '8',
            "comissao_sem_desbloqueio": '8',
            "comissao_diferenca_frete": '8',
            "frete_b2w_entrega": '9',
            "frete_promocional_b2w_entrega": '9',
            "diferenca_frete": '9',
            "estorno_frete_b2w_entrega": '10',
            "estorno_frete_promocional_b2w_entrega": '10',
            "participacao_frete": '9',
            "taxa_fixa": '82',
            "estorno_taxa_fixa": '10',
            "estorno_cobranca_b2w": '10',
            "pagamento_parcela": '21',
            "pagamento_parcela_flex": '21'
        }

        self.reference_correlation = {
            'Estorno_Venda': '1',
            'Estorno_Comissao': '2',
            'Estorno_Frete_B2W_Entrega': '3',
            'Venda': '4',
            'Comissao': '5',
            'Frete_B2W_Entrega': '6',
            'Comissao_Ressarcimento_Promocao': '7',
            'Antecipacao_Seguro': '8',
            'Participacao_frete': '9',
            'Taxa_fixa': '10',
            'Estorno_Taxa_fixa': '11',
            'Cashback_Ame': '12',
            'Ressarcimento_promocao': '13',
            'Estorno_Cashback_Ame': '15',
            'Diferenca_credito_desconto': '16',
            'Estorno_promocao': '17',
            'Tarifa_adicional': '18',
            'Despesa_Juridica': '19',
            'Despesa_inicial': '20',
            'IR': '21',
            'Estorno_Cobranca_B2W': '22',
            'Cobranca_B2W': '23',
            'Bonus': '24',
            'Subsidiaria_BIT': '25',
            'Comissao_Sem_Desbloqueio': '26',
            'Tarifa_Adicional': '27',
            'Liberacao_Reserva_Saldo': '28',
            'Diferenca_debito_desconto': '29',
            "Pagamento_Parcela": '30',
            "Pagamento_Parcela_Flex": '31'
        }

    @staticmethod
    def impeed_locked_register(msg, dic):

        if dic.get('status').lower() != "locked":
            return {"type": "inplace", "content": msg}

        return {"type": "ignore", "content": {}}

    @staticmethod
    def set_id_withdrawal(msg, dic):

        order_date = dic.get('data_prevista_pgto')

        date_concatenate = order_date.split('/')
        date_concatenate.reverse()
        date_concatenate = ''.join(date_concatenate)

        msg['id_withdrawal'] = date_concatenate

        return {"type": "inplace", "content": msg}

    def set_id_channel(self, msg, dic):

        msg['id_channel'] = self.id_channel_correlation.get(
            dic.get('marca'), 
            msg.get('id_channel')
        )

        return {"type": "inplace", "content": msg}

    def set_id_code(self, msg, dic):
        msg['id_code'] = self.id_code_correlation.get(
            dic.get('tipo').lower(), 
            '21'
        )

        if (
            msg.get('id_order') == '' or
            msg.get('id_order') is None
        ):
            msg['id_order'] = msg.get('id_withdrawal')

        if msg['id_code'] == '21':
            msg['description'] = dic.get('tipo')

        else:
            msg['description'] = self.trackcashRefferences.balance.get(msg.get('id_code'))

        # if msg['reference'] in ['16', '20', '21', '22', '23']:
        #     msg['id_order'] = msg.get('id_withdrawal')

        return {"type": "inplace", "content": msg}

    def set_reference(self, msg, dic):
        msg['reference'] = (
            msg.get('id_withdrawal', '') + 
            self.reference_correlation.get(dic.get('tipo'),'')
        )

        return {"type": "inplace", "content": msg}

    def _verify_data(self):
        if (
            self.trackcashServices.df.empty
        ):
            print('Arquivo sem dados.')
            self.empty = True
        
        else:
            self.empty = False
