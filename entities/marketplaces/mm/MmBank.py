from entities.Core import Core


class MmBank(Core):

    def __init__(self, path, readeable):
        self.new_balance = {}
        super().__init__(path, readeable, [self.get_id_withdrawal, self.check_status, self.get_id_code])

    def check_status(self, msg, dic):
        if dic.get('status') == 'Bloqueado':
            return {"type": 'ignore', "content": msg}

    def get_id_code(self, msg, dic):
        self.new_balance = {
            'Venda': '7', 
            'Comissão': '8', 
            'Estorno': '10', 
            'Saque': '17', 
            'Tarifa de Saque': '18', 
            'Imposto de Renda': '19'
        }

        msg['id_code'] = self.new_balance.get(dic.get('order_type'))
        return {"type": 'inplace', "content": msg}

    def get_id_withdrawal(self, msg, dic):
        id_withdrawal = dic.get('order_date').split('/')
        id_withdrawal.reverse()
        id_withdrawal = "".join([e for i, e in enumerate(id_withdrawal)])
        msg['id_withdrawal'] = id_withdrawal
        if dic.get('order_id') == "":
            msg['id_order'] = id_withdrawal
        return {"type": 'inplace', "content": msg}
