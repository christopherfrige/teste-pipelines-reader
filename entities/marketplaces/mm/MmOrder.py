from entities.Core import Core


class MmOrder(Core):

    def __init__(self, path, readeable):
        super().__init__(path, readeable, [self.check_nf])

    def check_nf(self, msg, dic):
        if dic.get('nf_number') == "" or dic.get('nf_number') is None:
            del msg['invoices']
            # del msg['invoices'][0]['invoice_id']
        return {"type": 'inplace', "content": msg}




