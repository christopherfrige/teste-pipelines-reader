from entities.Core import Core
from entities.marketplaces.cnova import common

class CnovaOrder(Core):
    def __init__(self, path, readeable):
        super().__init__(
            path, 
            readeable, 
            [
                self.adjust_id_channel
            ]
        )

        self.id_channel_correlation = common.id_channel_correlation
        self._verify_df_data()

    def adjust_id_channel(self, msg, dic):
        aux_msg = msg.copy()
        origem_mkp = dic.get('origem_mkp').lower()

        aux_msg['id_channel'] = self.id_channel_correlation.get(
            origem_mkp, 
            origem_mkp
        )

        return {"type": "inplace", "content": aux_msg}

    def _verify_df_data(self):
        if self.trackcashServices.df.empty:
            print('Arquivo sem dados.')
            self.empty = True
        
        else: 
            self.empty = False
