from entities.Core import Core

class CnovaBank(Core):
    def __init__(self, file_path, file_content):
        super().__init__(
            file_path, 
            file_content, 
            [
                self.set_id_withdrawal,
                self.set_reference,
                self.set_id_channel,
                self.extract_commission_register,
                self.set_description_id_code_21
            ]
        )

        self._verify_df_data()

        self.id_channel_correlation = {'3': '2', '2': '3', '4': '8'}
        self.id_code_correlation = {'7': '8', '10': '12'}

    @staticmethod
    def set_id_withdrawal(msg, dic):

        date = dic['data_vencimento']
        date_splited = date.split('/')
        date_splited.reverse()

        msg['id_withdrawal'] = ''.join(date_splited) + msg['reference']

        return {"type": "inplace", "content": msg}

    @staticmethod
    def set_reference(msg, dic):
        msg['reference'] = msg['id_withdrawal']
        return {"type": "inplace", "content": msg}

    def set_id_channel(self, msg, dic):

        cnova_id_channel, cnova_channel_name = dic.get('origem_mkp').replace(' ', '').split('-')
        msg['id_channel'] = self.id_channel_correlation.get(cnova_id_channel, msg['id_channel'])

        return {"type": "inplace", "content": msg}

    def extract_commission_register(self, msg, dic):

        if msg.get('id_code') in ('7', '10'):
            aux_msg = msg.copy()
            aux_msg['id_code'] = self.id_code_correlation.get(msg['id_code'])
            aux_msg['description'] = self.trackcashRefferences.balance.get(aux_msg['id_code'])
            aux_msg['total'] = -(dic.get('valor_comissao') * 100)
            return {"type": 'clone', 'content': aux_msg}

    @staticmethod
    def set_description_id_code_21(msg, dic):

        msg_copy = msg.copy()

        if msg['id_code'] == '21':
            msg_copy['description'] = dic.get('tipo_transacao')
        
        return {"type": 'inplace', "content": msg_copy}

    def _verify_df_data(self):
        if self.trackcashServices.df.empty:
            print('Arquivo sem dados.')
            self.empty = True
        
        else: 
            self.empty = False