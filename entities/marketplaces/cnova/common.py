from datetime import datetime

def adjust_id_store(msg, dic):
    if 'casas bahia' in dic.get('origem_mkp').lower():
        msg['id_channel'] = '2'
        return {"type":"inplace","content":msg}

    elif 'extra' in dic.get('origem_mkp').lower():
        msg['id_channel'] = '3'
        return {"type":"inplace","content":msg}

    elif 'ponto frio' in dic.get('origem_mkp').lower():
        msg['id_channel'] = '8'
        return {"type":"inplace","content":msg}

    return msg

def set_id_withdrawal(msg, dic, field_name):
    aux_msg = msg.copy()

    id_withdrawal = datetime.strptime(
        dic.get(field_name),
        '%d/%m/%Y'
    ).strftime(
        '%Y%m%d'
    )

    aux_msg['id_withdrawal'] = id_withdrawal

    if dic.get('order_id') == "":
        aux_msg['id_order'] = id_withdrawal

    return {"type": 'inplace', "content": aux_msg}

id_channel_correlation = {
    'casas bahia': '2',
    'extra': '3',
    'ponto frio': '8',
    'ponto': '8',
}