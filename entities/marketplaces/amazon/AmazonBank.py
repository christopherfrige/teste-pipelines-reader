from entities.Core import Core

class AmazonBank(Core):

    def __init__(self,path, readeable):
        super().__init__(path, readeable,[self.setIdCode,self.lockEmpty])

    def _defineTransactionType(self,msg,dic,idCorrelation):

        if dic.get('price_type') != '':
            msg['id_code'] = idCorrelation.get(dic.get('price_type'))

        else:
            msg['id_code'] = idCorrelation.get(dic.get('item_related_fee_type'))

        msg['description'] = self.trackcashRefferences.balance.get(msg.get('id_code'))

        return msg

    def lockEmpty(self,msg,dic):

        if dic.get('price_type') == '' and dic.get('item_related_fee_type') == '' and msg['id_code'] not in ('17','21'):
            msg = {}
        return {"type": 'inplace', "content": msg}

    def setIdCode(self,msg,dic):
        idCodeCorrelationSell = {'Principal': '7', 'Shipping': '7', 'Commission': '8', 'ShippingHB': '8'}
        idCodeCorrelationRefound = {'Principal': '10', 'Shipping': '10', 'Commission': '12', 'ShippingHB': '12','RefundCommission': '11'}

        if dic.get('transaction_type') == 'Order':
            msg = self._defineTransactionType(msg,dic,idCodeCorrelationSell)

        elif dic.get('transaction_type') == 'Refund':
            msg = self._defineTransactionType(msg,dic,idCodeCorrelationRefound)

        elif dic.get('transaction_type') == '':
            msg['id_order'] = msg['id_withdrawal']
            msg['id_code'] = '17'
            msg['description'] = self.trackcashRefferences.balance.get(msg.get('id_code'))
            msg['total'] = - float(dic.get('total_amount'))*100

        else:
            otherTypesRelation = {"Previous Reserve Amount Balance":'6',"Current Reserve Amount":'7',"BuyerRecharge":'8',"Subscription Fee":'9'}

            msg['id_order'] = msg['id_withdrawal']
            msg['id_code'] = '21'
            msg['description'] = dic.get('transaction_type')
            msg['total'] = float(dic.get('other_amount'))*100
            msg['reference'] = otherTypesRelation.get(dic.get('transaction_type'))

        return {"type":'inplace',"content":msg}


