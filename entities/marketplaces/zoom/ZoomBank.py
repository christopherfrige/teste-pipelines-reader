import re
from datetime import datetime, timedelta

from entities.Core import Core
from utilities.DateManipulator import DateManipulator

DATE_FORMAT = '%Y/%m/%d %H:%M'

class ZoomBank(Core):

    def __init__(self, path, readeable):
        self.new_balance = {}
        self.path = path
        self.transction_relation = {
            'Venda': '7-1',
            'Desconto concedido pelo Lojista': '21-2',
            'Receita/estorno sobre venda cancelada pelo lojista': '21-3',
            'Débito de comissão sobre venda cancelada pelo lojista': '21-4',
            'Estorno de desconto patrocinado pela Loja, por pedido cancelado pelo lojista': '21-5',
            'Receita/estorno sobre venda cancelada pelo Zoom': '21-6',
            'Débito de comissão sobre venda cancelada pelo Zoom': '21-7',
            'Ajuste manual de crédito ou débito': '21-8',
            'Estorno de desconto patrocinado pela Loja, por pedido cancelado pelo Zoom': '10-9',
            'Imposto de Renda': '21-10',
            'Comissão': '8-11',
            "Estorno": '10-12',
            "Receita/estorno": '12-13',
            "Desconto": '21-14'
        }

        super().__init__(
            path, 
            readeable, 
            [
                self.set_date,
                self.get_id_withdrawal,
                self.set_id_code,
                self.fix_desconto,
                self.adjust_transaction_type
            ]
        )

    def set_id_code(self, msg, dic):
        aux_msg = msg.copy()
        aux_msg['id_code'], aux_msg['reference'] = self.transction_relation.get(dic.get('transaction_type')).split("-")

        if aux_msg['id_code'] == "21":
            aux_msg['description'] = dic.get('transaction_type')
        else:
            aux_msg['description'] = self.trackcashRefferences.balance.get(aux_msg['id_code'])

        return {"type": 'inplace', "content": aux_msg}

    def adjust_transaction_type(self, msg, dic):
        aux_msg = msg.copy()

        if dic.get('transaction_type') == 'Venda':
            aux_msg['id_code'], aux_msg['reference'] = self.transction_relation.get('Comissão').split("-")
            aux_msg['total'] = -float(dic.get('comission_value')) * 100
            aux_msg['description'] = self.trackcashRefferences.balance.get(aux_msg['id_code'])
            return {"type": 'clone', "content": aux_msg}

    def get_id_withdrawal(self, msg:dict, dic):
        aux_msg = msg.copy()

        id_withdrawal = DateManipulator().convert_date(
            date=msg.get('date'),
            enter_format=DATE_FORMAT,
            exit_format='%Y%m%d'
        )

        aux_msg['id_withdrawal'] = id_withdrawal
        if dic.get('transaction_type') == "Imposto de Renda":
            aux_msg['id_order'] = id_withdrawal

        return {"type": 'inplace', "content": aux_msg}

    def set_date(self, msg, dic):
        aux_msg = msg.copy()

        date_data = self._handle_get_date_from_name()
        
        if not date_data:
            raise Exception('Erro - Zoom Bank - get date from file name')

        aux_msg["date"] =  date_data

        return {"type": 'inplace', "content": aux_msg}

    def _handle_get_date_from_name(self):
        file_name = self.path.split("/")[-1].split(".")[0]

        date_timestamp = None

        data = self._test_regex_date(
            regex=r'20[0-2][0-9](/|-|_)((0[1-9])|(1[0-2]))(/|-|_)([0-2][1-9]|10|20|3[0-1])', 
            data=file_name
        )

        if data:
            data = data.group().replace('/', '-').replace('_', '-')
            date_timestamp = datetime.strptime(data, "%Y-%m-%d")

        data = self._test_regex_date(
            regex=r'(([1-9] |1[0-9]| 2[0-9]|3[0-1])(/|-|_)([1-9] |1[0-2])(/|-|_)20[0-9][0-9])', 
            data=file_name
        )

        if data:
            data = data.group().replace('/', '-').replace('_', '-')
            date_timestamp = datetime.strptime(data, "%d-%m-%Y")
        
        if not date_timestamp:
            print(f'Date formatting outside the agreed standards - {file_name}')
            return

        date_timestamp_plus = date_timestamp + timedelta(days=3)
        return date_timestamp_plus.strftime(DATE_FORMAT)

    def _test_regex_date(self, regex, data):
        return re.search(regex, data)

    def fix_desconto(self, msg, dic):
        aux_dic = dic.copy()
        aux_msg = msg.copy()
        if dic.get("transaction_type") == "Desconto concedido pelo Lojista":
            aux_dic["cycle_value"] = round(-float(dic.get("cycle_value")), 3)*100
            aux_msg["total"] = aux_dic["cycle_value"]

        return {"type": 'inplace', "content": aux_msg}
