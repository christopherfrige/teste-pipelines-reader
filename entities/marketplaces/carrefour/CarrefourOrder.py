from entities.Core import Core

class CarrefourOrder(Core):

    def __init__(self, path, content):
        super().__init__(
            path, 
            content, 
            [
                self.verify_invoice_data
            ]
        )

        self._verify_df_data()

    def verify_invoice_data(self, msg, dic):
        invoice_id = (
            msg
            .get('invoices')[0]
            .get('invoice_id')
        )

        if not invoice_id:
            del msg['invoices']

        return {"type":"inplace","content":msg}

    def _verify_df_data(self):
        if self.trackcashServices.df.empty:
            print('Arquivo sem dados.')
            self.empty = True
        
        else: 
            self.empty = False