from entities.Core import Core
import datetime

class CarrefourBank(Core):
    def __init__(self, path, content):
        super().__init__(
            path, 
            content, 
            [
                self.adjust_cycle_total_value, 
                self.set_date_in_carrefour_format,
                self.extract_commission_register,
                self.set_description_id_code_21
            ]
        )

    @staticmethod
    def adjust_cycle_total_value(msg, dic):
        if msg.get('id_code') == '17':
            msg['total'] = -(msg['total'])
        return {"type": 'inplace', "content": msg}

    def extract_commission_register(self, msg, dic):

        if msg.get('id_code') in ('7', '10'):
            aux_msg = msg.copy()
            aux_msg['id_code'] = self.id_code_commission_correlation.get(msg['id_code'])
            aux_msg['description'] = self.trackcashRefferences.balance.get(msg['id_code'])
            aux_msg['total'] = -(dic.get('comissao') * 100) if dic.get('comissao') != '' else 0
            return {"type": 'clone', 'content': aux_msg}

    @staticmethod
    def same_month_case(date, days):
        receipt_date = date + datetime.timedelta(days=days)
        output_date = datetime.datetime.strftime(receipt_date, "%Y-%m-%d - %H:%M:%S")

        return output_date

    @staticmethod
    def different_month_case(date, day):
        new_month = date.month + 1
        year = date.year

        if new_month > 12:
            new_month = new_month - 12
            year += 1

        payment_date = datetime.datetime(year, new_month, day)

        payment_date_string_format = datetime.datetime.strftime(payment_date, "%Y-%m-%d - %H:%M:%S")

        return payment_date_string_format

    def set_date_in_carrefour_format(self, msg, dic):
        date = dic.get('Período').split('-')[-1].strip()

        date_in_datetime_format = datetime.datetime.strptime(date, '%d/%m/%Y')

        if date_in_datetime_format.day == 11:
            msg['date'] = self.same_month_case(date_in_datetime_format, 9)
        else:
            msg['date'] = self.different_month_case(date_in_datetime_format, 10)

        return {"type": 'inplace', "content": msg}

    @staticmethod
    def set_description_id_code_21(msg, dic):

        msg_copy = msg.copy()

        if msg['id_code'] == '21':
            msg_copy['description'] = dic.get("tipo")

        return {"type": 'inplace', "content": msg_copy}
