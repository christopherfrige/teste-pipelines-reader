from entities.Core import Core


class CarrefourBankOld(Core):

    def __init__(self, path, content):
        super().__init__(
            path, 
            content, 
            [
                self.set_description_id_code_21
            ]
        )

    @staticmethod
    def set_description_id_code_21(msg, dic):
        msg_copy = msg.copy()

        if msg['id_code'] == '21':
            msg_copy['description'] = dic.get("tipo")

        return {"type": 'inplace', "content": msg_copy}