from entities.Core import Core

class GpaBank(Core):

    def __init__(self,path,readeable):
        super().__init__(
            path,
            readeable,
            [
                self.setDescriptionId21
            ]
        )
    
    def setDescriptionId21(self, msg, dic):
        
        msg_copy = msg.copy()

        if msg['id_code'] == '21':
            msg_copy['description'] = dic.get('tipo')
        
        return {"type": 'inplace', "content": msg_copy}