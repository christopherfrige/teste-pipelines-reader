from entities.Core import Core
from .commons import set_id_channel as set_id_channel_common


class NetshoesOrder(Core):

    def __init__(self, path, readeable):
        super().__init__(
            path, 
            readeable, 
            [
                self.set_id_channel
            ]
        )

        self._verify_df_data()

    @staticmethod
    def set_id_channel(msg, dic):
        res = set_id_channel_common(dic.get('site_venda'))
        msg['id_channel'] = res

        return {"type": 'inplace', "content": msg}

    def _verify_df_data(self):
        if self.trackcashServices.df.empty:
            print('Arquivo sem dados.')
            self.empty = True
        
        else: 
            self.empty = False