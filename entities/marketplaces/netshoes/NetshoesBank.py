from entities.Core import Core
from .commons import set_id_channel as set_id_channel_common

from utilities.DateManipulator import DateManipulator

class NetshoesBank(Core):

    def __init__(self, path, readeable):
        self.repasse_date = None

        super().__init__(
            path=path, 
            readeableContent=readeable, 
            functionsSignature=[
                self.set_id_channel,
                self.set_id_withdrawal, 
                self.set_reference, 
                self.get_comission,
                self.set_shipment, 
                self.block_cancelado
            ]
        )

        self._verify_df_data()
        
    @staticmethod
    def block_cancelado(msg, dic):
        if dic.get('status_pedido') == 'Cancelado':
            return {"type": 'inplace', "content": {}}

    def get_comission(self, msg, dic):
        msg_copy = msg.copy()

        if msg.get('id_code') == '7' and dic.get('status_pedido') != "Cancelado":

            msg_copy['total'] = - dic.get('valor_comissao')*100
            msg_copy['id_code'] = '8'
            msg_copy['description'] = self.trackcashRefferences.balance.get(msg_copy.get('id_code'))

            return {"type": 'clone', "content": msg_copy}

    @staticmethod
    def set_id_channel(msg, dic):
        res = set_id_channel_common(dic.get('site_origem_venda'))
        msg['id_channel'] = res

        return {"type": 'inplace', "content": msg}

    @staticmethod
    def set_id_withdrawal(msg, dic):
        aux_msg = msg.copy()

        id_withdrawal = DateManipulator().convert_date(
            date=msg.get('date'),
            enter_format='%Y-%m-%d - %H:%M:%S',
            exit_format='%Y%m%d'
        )

        aux_msg['id_withdrawal'] = id_withdrawal

        if dic.get('order_id') == "":
            aux_msg['id_order'] = id_withdrawal
            
        return {"type": 'inplace', "content": aux_msg}

    @staticmethod
    def set_reference(msg, dic):
        aux_msg = msg.copy()

        if dic.get('nr_pedido_netshoes') == "":
            aux_msg['reference'] = aux_msg['id_withdrawal']
        else:
            aux_msg['reference'] = dic.get('nr_pedido_netshoes')

        return {"type": 'inplace', "content": aux_msg}

    # Registro do tipo 9 com valor negativo

    def set_shipment(self,msg,dic):
        aux_msg = msg.copy()

        if msg['id_code'] == '7' and dic.get('plataforma') == 'NETSHOES_ENTREGAS' and dic.get('status_pedido') != "Cancelado":
            aux_msg['id_code'] = '9'
            aux_msg['total'] = - dic.get('valor_total_frete_lojista') * 100
            aux_msg['description'] = self.trackcashRefferences.balance.get(aux_msg.get('id_code'))

            return {"type": 'clone', "content": aux_msg}

    def _verify_df_data(self):
        if self.trackcashServices.df.empty:
            print('Arquivo sem dados.')
            self.empty = True
        
        else: 
            self.empty = False