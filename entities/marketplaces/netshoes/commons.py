
def set_id_channel(origem_venda):
    id_channel_correlation = {
        "NETSHOES": 15,
        "ZATTINI": 16,
        "SHOPTIMÃO": 17,
        "KAPPA": 438,
        "ALLIANZ PARQUE": 439,
        "SANTOS": 440,
    }

    res = id_channel_correlation.get(str(origem_venda).upper())
    return res
