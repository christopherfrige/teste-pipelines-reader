from entities.Core import Core
from entities.marketplaces.cnova import common

class ViaVarejoBankCard(Core):
    def __init__(self, file_path, file_content):
        super().__init__(
            file_path, 
            file_content, 
            [
                self.get_id_channel,
                self.set_id_withdrawal,
                self.set_reference,
                self.set_adjust_id_code,
                self.extract_commission_register,
                self.adjust_transfer_values,
                self.set_description_id_code_21
            ]
        )

        self._verify_df_data()

        self.channel_correlation = common.id_channel_correlation
        self.adjust_id_code_correlation = {
            'cancelamento do lojista': '11', 
            'acao judicial': '19'
        }

    def get_id_channel(self, msg, dic):
        name_site =  dic.get('nome_site_via_varejo').lower()
        msg['id_channel'] = self.channel_correlation.get(
            name_site, 
            '13'
        )

        return {"type": "inplace", "content": msg}

    @staticmethod
    def set_id_withdrawal(msg, dic):

        date = dic['data_efetivacao_pagamento']
        date_splited = date.split('/')
        date_splited.reverse()

        msg['id_withdrawal'] = ''.join(date_splited) + msg['reference']

        return {"type": "inplace", "content": msg}

    @staticmethod
    def set_reference(msg, dic):
        msg['reference'] = msg['id_withdrawal']
        return {"type": "inplace", "content": msg}

    def adjust_transfer_values(self, msg, dic):

        installment_value = dic.get('valor_parcela') * 100 if dic.get('valor_parcela') else 0.0

        adjust_calc = (dic.get('valor_do_repasse') * 100 -
                       (installment_value - dic.get('valor_da_comissao_via_varejo') * 100))

        if adjust_calc != 0 and msg.get('id_code') == '11':
            aux_msg = msg.copy()
            aux_msg['id_code'] = '21'
            aux_msg['description'] = self.trackcashRefferences.balance.get(aux_msg.get('id_code'))
            aux_msg['total'] = adjust_calc
            return {"type": 'clone', 'content': aux_msg}

    def extract_commission_register(self, msg, dic):

        if msg.get('id_code') in ('7', '10'):

            aux_msg = msg.copy()
            aux_msg['id_code'] = self.id_code_commission_correlation.get(msg['id_code'])
            aux_msg['description'] = self.trackcashRefferences.balance.get(aux_msg.get('id_code'))

            if aux_msg['id_code'] == '8':
                commission_value = -(dic.get('valor_da_comissao_via_varejo') * 100)

            elif aux_msg['id_code'] == '12' and dic.get('valor_parcela') != 0.0:
                commission_value = - (dic.get('valor_parcela') - dic.get('valor_do_repasse')) * 100

            else:
                commission_value = - (dic.get('valor_da_transacao') - dic.get('valor_do_repasse')) * 100

            aux_msg['total'] = commission_value
            return {"type": 'clone', 'content': aux_msg}

    def set_adjust_id_code(self, msg, dic):
        if dic.get('titulo_do_ajuste') != '':
            msg['id_code'] = self.adjust_id_code_correlation.get(dic.get('titulo_do_ajuste').lower(), '21')

            msg['total'] = - (dic.get('valor_da_comissao_via_varejo') * 100)
            msg['description'] = self.trackcashRefferences.balance.get(msg.get('id_code'))

        return {"type": "inplace", "content": msg}

    @staticmethod
    def set_description_id_code_21(msg, dic):

        msg_copy = msg.copy()

        if msg['id_code'] == '21':

            if dic.get('tipo_da_transacao') != 'AJUSTE':
                msg_copy['description'] = dic.get('tipo_da_transacao')
            else:
                msg_copy['description'] = dic.get('titulo_do_ajuste')

        return {"type": 'inplace', "content": msg_copy}

    def _calc_total(self, dic):
        installmentValue = (
            dic.get('valor_parcela') * 100 
            if dic.get('valor_parcela') 
            else 0.0
        )

        installmentValueLessComission = (
            installmentValue - dic.get('valor_da_comissao_via_varejo')
        )

        return (
            (dic.get('valor_do_repasse') - installmentValueLessComission) * 100
        ) 

    def _verify_df_data(self):
        if (
            self.trackcashServices.df.empty or
            not (
                sum(
                    self.trackcashServices.df['Número do Pedido'].to_list()
                )
            )
        ):
            print('Arquivo sem dados.')
            self.empty = True
        
        else: 
            self.empty = False
