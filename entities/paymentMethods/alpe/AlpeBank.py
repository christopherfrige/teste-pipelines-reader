from entities.Core import Core
import datetime

class AlpeBank(Core):

    def __init__(self, path, content):
        super().__init__(
            path, 
            content, 
            [
                self.set_id_channel,
                self.set_reference,
                self.change_to_int_id_order,
                self.set_id_code_22_date,
                self.set_register_as_id_code_21,
                self.change_id_code_21_structure,
                self.extractInstallmentsInfos,
                self.extractIdWithdrawal,
                self.extractComission
            ]
        )

        self.idWithdrawalAdjustBank = []
        self.reference_table = {
            'ajuste':'1',
            'crédito a vista':'2',
            'crédito parcelado':'3',
            'débito':'4',
            'financiamento':'6'
        }

    @staticmethod
    def change_to_int_id_order(msg, dic):

        try:
            msg['id_order'] = int(msg['id_order'])
            msg['reference'] = msg['id_order']
        except:
            pass

        return {"type": 'inplace', 'content': msg}

    def set_reference(self, msg, dic):
        msg['reference'] = self.reference_table.get(dic.get('tipo_transacao','').lower())

        return {"type": 'inplace', 'content': msg}

    @staticmethod
    def set_id_channel(msg, dic):
        msg['id_channel'] = '431'

        return {"type": 'inplace', 'content': msg}

    @staticmethod
    def set_id_code_22_date(msg, dic):

        if msg['id_code'] == '22':
            msg['date'] = datetime.datetime.strptime(dic.get('previsao_pagamento'),'%d/%m/%Y').strftime('%Y-%m-%d - %H:%M:%S')

        return {"type": 'inplace', 'content': msg}

    @staticmethod
    def set_register_as_id_code_21(msg, dic):

        if dic.get('tipo_transacao') in ['Financiamento','Ajuste']:
            msg['id_code'] = '21'
        return {"type": 'inplace', 'content': msg}

    @staticmethod
    def change_id_code_21_structure(msg, dic):

        if msg['id_code'] == '21':
            date_msg = dic.get('data_operacao', '').split(' ')[0].split('/')
            date_msg.reverse()
            date_msg = ''.join(date_msg)
            msg['id_order'] = date_msg

            msg['description'] = dic.get('tipo_transacao')

            return {"type": 'inplace', 'content': msg}

    def extractInstallmentsInfos(self, msg, dic):
        installmentsField = dic.get('plano')

        if installmentsField == '':
            installmentsField = '1/1'

        currentInstallment, totalInstallments = installmentsField.replace('(', '').replace(')', '').split('/')

        msg['installments'] = totalInstallments
        msg['current_installment'] = currentInstallment

        return {"type": 'inplace', 'content': msg}

    def extractIdWithdrawal(self, msg, dic):
        dateFramgments = dic.get('data_pagamento').split('/')
        dateFramgments.reverse()

        idWithdrawal = ''.join(dateFramgments)

        msg['id_withdrawal'] = idWithdrawal

        return {"type": 'inplace', 'content': msg}

    def extractComission(self, msg, dic):
        aux_msg = msg.copy()
        aux_msg['total'] = - (dic.get('valor_parcela') - dic.get('valor_liquido')) * 100

        if msg['id_code'] in ('7', '71'):
            aux_msg['id_code'] = '81'
            aux_msg['description'] = self.trackcashRefferences.balance.get(aux_msg['id_code'])
            return {"type": 'clone', 'content': aux_msg}

        elif msg['id_code'] in ('233','22'):
            aux_msg['id_code'] = '231'
            aux_msg['description'] = self.trackcashRefferences.balance.get(aux_msg['id_code'])
            return {"type": 'clone', 'content': aux_msg}

        del aux_msg
        return {"type": 'inplace', 'content': msg}