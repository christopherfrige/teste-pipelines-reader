from entities.Core import Core


class AlpeOrder(Core):

    def __init__(self, path, readeable):
        super().__init__(path, readeable, [self.adjustIdChannel,self.change_to_int_id_order,self.extractInstallment])

    @staticmethod
    def change_to_int_id_order(msg, dic):

        try:
            msg['id_order'] = int(msg['id_order'])
            msg['payments'][0]['payment_id'] = int(msg['payments'][0]['payment_id'])
            msg['reference'] = msg['id_order']
        except:
            pass

        return {"type": 'inplace', 'content': msg}

    def adjustIdChannel(self, msg, dic):
        msg['id_channel'] = '431'

        msg['payments'][0]['payment_method'] = msg['id_channel']

        return {"type": 'inplace', 'content': msg}

    def extractInstallment(self, msg, dic):

        if 'crédito parcelado' in dic.get('tipo_transacao').lower():
            serviceRefference = {'débito': '3', 'débito parcelado': '4', 'crédito parcelado': '10',
                                 'crédito à vista': '15', 'voucher': '16', 'voucher parcelado': '17', 'boleto': '19'}

            transactionTypeFragments = dic.get('tipo_transacao', '').split(' ')
            paymentService = ' '.join(transactionTypeFragments[:2])
            installments = transactionTypeFragments[-1][0]

            msg['payments'][0]['payment_service'] = serviceRefference.get(paymentService.lower())
            msg['payments'][0]['payment_installments'] = installments

        return {"type": 'inplace', 'content': msg}
