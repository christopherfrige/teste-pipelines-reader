from services.TrackcashServices import TrackcashServices
from services.TrackcashRefferences import TrackcashRefferences
from utilities.DateManipulator import DateManipulator
from services.SlackBot import SendErrorMessage
from math import ceil
import datetime

class Core:

    def __init__(self,
        path,
        readeableContent,
        functionsSignature,
    ):
        self.path = path
        self.fragmentedPath = path.replace('.reader','').split('/')
        self.idStore = self.fragmentedPath[2]
        self.processName = self.fragmentedPath[4]
        self.idChannel = self.fragmentedPath[3]
        self.accountNumber = self.fragmentedPath[5]
        self.date = self.fragmentedPath[-2]
        self.idArchieve = self.fragmentedPath[-1]

        self.trackcashRefferences = TrackcashRefferences(self.idStore,self.idChannel)
        
        self.trackcashServices = TrackcashServices(
            configFile=self.processName,
            readeableContent=readeableContent,
            contentFile=self.idArchieve,
        )
        
        self.dateManipulator = DateManipulator()

        self.functionSignature = functionsSignature

        self.id_code_commission_correlation = {'7': '8', '10': '12'}

    def _setFileStatus(self,status):
        self.trackcashServices.setReadingFileStatus(self.idStore,self.path,status)

    def _splitArray(self,array):

        delimiter = 20000
        messagesPacker = []

        if len(array) > delimiter:
            
            arraysTotal = ceil(len(array)/delimiter)

            first,last = (0,delimiter)
            for i in range(arraysTotal):

                if i != arraysTotal:
                    package = array[first:last]
                    messagesPacker.append(package)

                    first += delimiter
                    last += delimiter        

                else:
                    package = array[first:-1]
                    messagesPacker.append(package)        
        else:
            messagesPacker.append(array)

        for package in messagesPacker:
            code,response = self.trackcashServices.bodyWorker(package,self.trackcashServices.config.get('process'))
            if code != 200:
                return code,response

        response = "Sucess"

        return code,response

    #TODO:Revisar todas as operações com DataFrame, consumo excessivo de memória no AWS Lambda
    def readFile(self):
        if self.trackcashServices.config == 'file error':
            print('Cancel CSV because cannot open config file')
            return

        # contador_de_dados = self.df.count()
        # # print(self.df)
        # # print(contador_de_dados)
        
        self.trackcashServices.df = self.trackcashServices.df.rename(
            columns=self.trackcashServices.config.get('columns_name')
        )

        self.trackcashServices.df = self.trackcashServices.df.fillna('')

        self.trackcashServices.convertRules(
            self.trackcashServices.config.get('change_type')
        )

        list_msg = []

        for params in self.trackcashServices.config.get('params'):
            aux = self.trackcashServices.df

            remove_duplicate = False
            filters = params.get('sum_duplicates', {}).get('filters', [])

            for c_sum in params.get('sum_duplicates', {}).get('sum'):

                if not filters:
                    continue
                remove_duplicate = True

                try:
                    aux[c_sum] = aux.groupby(filters)[c_sum].transform('sum')
                except:
                    pass

            if remove_duplicate:
                aux = aux.drop_duplicates(subset=filters)
            
            list_dic = aux.to_dict('records')
            for dic in list_dic:
                for blueprint in params.get('blueprint'):
                    msg = {
                        "id_store": self.idStore,
                        "channel": self.idChannel,
                        "id_account": self.accountNumber,
                        "id_channel":self.trackcashRefferences.idChannel.get(self.idChannel)
                    }

                    if eval(blueprint.get('condition')):
                        for key, value in blueprint.items():

                            if key not in ["condition", "kafka_topic"]:

                                if type(value) is list:
                                    processedValues = {}

                                    for item in value[0]:
                                        try:
                                            processedValues[item] = eval(value[0][item])
                                        except Exception as error:
                                            print(error)
                                            send_msg = SendErrorMessage(msg={"channel":self.idChannel,"id_store":self.idStore,"path":self.path,"error":error})
                                            send_msg.start()
                                            self._setFileStatus('Erro')
                                            return

                                    msg[key] = [processedValues]

                                else:
                                    try:
                                        msg[key] = eval(value)
                                    except Exception as error:
                                        print(error)
                                        send_msg = SendErrorMessage(msg={"channel":self.idChannel,"id_store":self.idStore,"path":self.path,"error":error})
                                        send_msg.start()
                                        self._setFileStatus('Erro')
                                        return


                    msg['id_archive'] = self.idArchieve

                    for function in self.functionSignature:

                        try:
                            newMessage = function(msg,dic)

                        except Exception as error:
                            print(error)
                            send_msg = SendErrorMessage(msg={"channel": self.idChannel, "id_store": self.idStore, "path": self.path,"error":error})
                            send_msg.start()
                            self._setFileStatus('Erro')
                            return

                        if newMessage != None:
                            if newMessage.get('type') == 'clone':
                                list_msg.append(newMessage.get('content'))

                            elif newMessage.get('type') == 'ignore':
                                msg = {}
                                break

                            else:
                                msg = newMessage.get('content')

                    if not msg: continue

                    list_msg.append(msg)
                    pass
                    
        code, response = self._splitArray(list_msg)
        
        if code == 200:
            self._setFileStatus('Lido')
        else:
            send_msg = SendErrorMessage(msg={"channel":self.idChannel,"id_store":self.idStore,"path":self.path,"error":f"API error,status:{code}"})
            send_msg.start()
            self._setFileStatus('Erro')

        return code,response