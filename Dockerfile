# Usa uma imagem base do python3.6 slim buster
# ELa contém bibliotecas básicas para o funcionamento do serverless-python-requirements
FROM python:3.6-slim-buster

# Cria uma pasta para a instalação das bibliotecas pelo apt-get
WORKDIR /deploy_setup

# Instalação de bibliotecas essenciais
RUN apt-get update  \
    && apt-get install git -y \
    && apt-get install curl -y \
    && apt-get install bash -y  

# Instalação do node e do npm, para a execução do serverless framework
RUN apt-get install nodejs -y  \
    && apt-get install npm -y \
    && npm i -g serverless 