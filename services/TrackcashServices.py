import os
import json
import pandas
import requests
import numpy as np

from utilities.DateManipulator import DateManipulator
from utilities.EnviromentVariables import ENVIRONMENT_TYPE
from utilities.GlobalVariables import (
    URL_API_ORDERS_TESTE,
    URL_API_REPASSE_TESTE,
    HEADER_TRACKCASH,
    URL_API_ORDERS_PROD,
    URL_API_REPASSE_PROD,
    URL_API_ERRORS,
    URL_API_STATUS_FILE_PROD,
    URL_API_STATUS_FILE_TEST
)

class TrackcashServices:
    def __init__(
            self,
            configFile=None,
            readeableContent=None,
            contentFile=None,
        ):

        self.config = (
            self.getConfig(configFile) 
            if configFile != None 
            else None
        )
        
        self.df = (
            self.readFile(
                readeableContent,
                contentFile,
                configFile
            ) 
            if configFile != None 
            else None
        )

    def convertRules(self,rules):
        if not rules:
            return self.df

        for rule in rules:
            if rule.get('type') in ['integer', 'signed', 'unsigned', 'float','str']:
                self.df[rule.get('column')] = pandas.to_numeric(
                    self.df[rule.get('column')],
                    downcast=rule.get('type'),
                    errors='coerce'
                )

            elif rule.get('type') in ['datetime']:
                self.df[rule.get('column')] = pandas.to_datetime(
                    self.df[rule.get('column')],
                    format=rule.get('format'),
                    errors='ignore'
                )

            elif rule.get('type') in ['str_eval']:
                self.df[rule.get('column')] = self.df[rule.get('column')].apply(
                    lambda x: eval(rule.get('command')) if type(x) is str else x
                )

    def _strip_file(self,frame,columns_refference,data_range):
        newSetOfColumns = frame.values.tolist()[columns_refference]
        frame.drop(frame.index[data_range], inplace=True)
        frame.columns = newSetOfColumns
        return frame

    def _strip_file_v2(self, df, check_options):
        index = 0
        while True and index <= 20:
            row = df.iloc[0].to_dict()

            if check_options(row):
                df.drop([index], axis=0, inplace=True)
                return df

            else:
                df.drop([index], axis=0, inplace=True)
                df.columns = df.values.tolist()[0]
                index += 1
        
        return False

    @staticmethod
    def _check_columns_netshoes_bank(row):
        return (
            row.get('Nr Pedido Netshoes') and
            row.get('Item do Pedido') and
            row.get('SKU Lojista') and
            row.get('qtde do SKU')
        )

    def _handle_get_repasse_date(self, df):
        date_info = list(df.iloc[0].to_dict().keys())[1]
        end_date = date_info.replace(' ', '').split('à')[1]

        return DateManipulator().repasse_date_to_netshoes(
            date=end_date,
            enter_format='%d/%m/%Y',
        )

    def _get_conversion_on_open_rules(self):
        rules = self.config.get('change_type_on_open')
        if rules:
            for key in rules:
                rules[key] = eval(rules[key])
        return rules

    def readFile(self,readeableContent,contentFile,configFile):
        extension = contentFile.split('.')[-1]
        conversion_on_open_rules = self._get_conversion_on_open_rules()
        if extension.lower() == 'csv':

            if 'bank_repasse_b2w' in configFile:
                dataFrameB2w = pandas.read_csv(readeableContent,sep=';',encoding='ISO-8859-1',dtype=conversion_on_open_rules)
                dataFrameB2w.loc[dataFrameB2w['Tipo'] == "IR","Entrega"] = dataFrameB2w['Data Prevista Pgto']
                return dataFrameB2w

            if 'b2w' in configFile:
                return pandas.read_csv(readeableContent, sep=';', encoding='ISO-8859-1',dtype=conversion_on_open_rules)

            if 'mm' in configFile:
                return pandas.read_csv(readeableContent, sep=';', encoding='ISO-8859-1',dtype=conversion_on_open_rules)

            if 'bank_alpe' in configFile:
                return pandas.read_csv(readeableContent, sep=',', dtype=conversion_on_open_rules)

            return pandas.read_csv(readeableContent,sep=';',dtype=conversion_on_open_rules)

        elif extension.lower() == 'xlsx' or extension.lower() == 'xls':

            if 'order_integra_magazine' in configFile:
                dataFrameMagazine =  pandas.read_excel(readeableContent,dtype=conversion_on_open_rules)
                if dataFrameMagazine.values.tolist()[0][0] == 'Informações do Pedido':
                    dataFrameMagazine = self._strip_file(dataFrameMagazine,1,[0,1])
                    self.config = self.getConfig('blue_order_integra_magazine') if configFile != None else None
                    return dataFrameMagazine

            if 'bank_netshoes' in configFile:
                dataFrameNetshoes = pandas.read_excel(readeableContent,dtype=conversion_on_open_rules)
                
                self.repasse_date = self._handle_get_repasse_date(
                    df=dataFrameNetshoes
                )
                
                dataFrameNetshoes = self._strip_file_v2(
                    df=dataFrameNetshoes,
                    check_options=self._check_columns_netshoes_bank
                )

                return dataFrameNetshoes

            if 'bank_magazine' in configFile:
                dataFrameMagazine = pandas.read_excel(readeableContent,dtype=conversion_on_open_rules)
                dataFrameMagazine.loc[dataFrameMagazine['Método de pagamento'] == "Evento","ID do pedido Magazine Luiza"] = dataFrameMagazine["ID da transação"]
                return dataFrameMagazine

            return pandas.read_excel(readeableContent,dtype=conversion_on_open_rules)

        elif extension.lower() == 'txt':

            return pandas.read_csv(readeableContent,encoding='utf-8',delimiter='\t',dtype=str)

    def getFoldersChannels(self,type):

        path = os.path.join(os.getcwd(),'entities',type)

        for itinerator in os.walk(path):
            return itinerator[1]

    def setReadingFileStatus(self,id_store,archive,status):
        if ENVIRONMENT_TYPE == 'PROD':
            URL_STATUS_FILE = URL_API_STATUS_FILE_PROD
            URL_STATUS_FILE = URL_API_STATUS_FILE_PROD
        else:
            URL_STATUS_FILE = URL_API_STATUS_FILE_TEST
            URL_STATUS_FILE = URL_API_STATUS_FILE_TEST

        postBody = {
            "id_store":id_store,
            "locale":archive,
            "status":status
        }

        result = requests.put(
            URL_STATUS_FILE,
            json=postBody,
            headers=HEADER_TRACKCASH
        )

        if result.status_code != 200:
                print(f'Error on send reading status, code:{result.status_code}')

    def getConfig(self,configFile):

        marketplaces = self.getFoldersChannels('marketplaces')
        paymentMethods = self.getFoldersChannels('paymentMethods')
        channelName = configFile.split('_')[-1]
        channelType = 'marketplaces' if channelName in marketplaces else 'paymentMethods'
        configPath = os.path.join(os.getcwd(),'static',channelType,channelName,configFile+'.json')
        #configPath = os.environ['LAMBDA_TASK_ROOT'] + '/static/' + configFile+'.json'
        try:
            with open(configPath, 'r',encoding='utf-8') as config:
                data = json.load(config)
                config.close()
                return data
        except Exception as e:
            print(e)
            return 'file error'

    def postBody(self, body, url):
        result = requests.post(
            url, 
            json=body, 
            headers=HEADER_TRACKCASH
        )

        return result.status_code, result.content

    def bodyWorker(self,body,process):
        if ENVIRONMENT_TYPE == 'PROD':
            URL_REPASSE = URL_API_REPASSE_PROD
            URL_ORDERS = URL_API_ORDERS_PROD
        else:
            URL_REPASSE = URL_API_REPASSE_TESTE
            URL_ORDERS = URL_API_ORDERS_TESTE
        
        if process:
            if 'order' in process:
                return self.postBody(body, URL_ORDERS)
            elif 'repasse' in process or 'transaction' in process or 'bank' in process or 'transfer' in process:
                return self.postBody(body, URL_REPASSE)
        else:
            print("Unable to use process")

    def sendErrors(self,message):
        result = requests.post(
            url=URL_API_ERRORS,
            json=message
        )
        if result.status_code == 200:
            return {'200':True}
        else:
            return {result.status_code:False}