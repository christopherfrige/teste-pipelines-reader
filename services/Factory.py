from services.Bucket import Bucket
from entities.marketplaces.carrefour.CarrefourOrder import CarrefourOrder
from entities.marketplaces.carrefour.CarrefourBank import CarrefourBank
from entities.marketplaces.carrefour.CarrefourBankOld import CarrefourBankOld
from entities.marketplaces.cnova.CnovaOrder import CnovaOrder
from entities.marketplaces.cnova.CnovaBank import CnovaBank
from entities.marketplaces.cnova.CnovaBankDetalhado import CnovaBankDetalhado
from entities.marketplaces.viavarejo.ViaVarejoOrder import ViaVarejoOrder
from entities.marketplaces.viavarejo.ViaVarejoBankBoleto import ViaVarejoBankBoleto
from entities.marketplaces.viavarejo.ViaVarejoBankCard import ViaVarejoBankCard
from entities.marketplaces.magazine.MagazineOrder import MagazineOrder
from entities.marketplaces.magazine.MagazineBank import MagazineBank
from entities.marketplaces.b2w.B2wOrder import B2wOrder
from entities.marketplaces.b2w.B2wBank import B2wBank
from entities.marketplaces.leroy.LeroyBank import LeroyBank
from entities.paymentMethods.alpe.AlpeOrder import AlpeOrder
from entities.paymentMethods.alpe.AlpeBank import AlpeBank
from entities.marketplaces.amazon.AmazonBank import AmazonBank
from entities.marketplaces.amazon.AmazonOrder import AmazonOrder
from entities.marketplaces.netshoes.NetshoesOrder import NetshoesOrder
from entities.marketplaces.netshoes.NetshoesBank import NetshoesBank
from entities.marketplaces.dafiti.DafitiBank import DafitiBank
from entities.marketplaces.mm.MmOrder import MmOrder
from entities.marketplaces.mm.MmBank import MmBank
from entities.marketplaces.zoom.ZoomBank import ZoomBank
from entities.marketplaces.gpa.GpaBank import GpaBank
from entities.marketplaces.gpa.GpaOrder import GpaOrder


class Factory:

    def __init__(self, file_path, bucket, process):
        self.file_path = file_path
        self.file_content = Bucket(bucket).get_file(file_path)
        self.process = process

    def start(self):
        signature_shelf = {
            "order_carrefour": CarrefourOrder,
            "transfer_carrefour": CarrefourBank,
            "transaction_carrefour": CarrefourBankOld,
            "order_cnova": CnovaOrder,
            "bank_cnova": CnovaBank,
            "bank_pago_detalhado_cnova": CnovaBankDetalhado,
            "order_viavarejo": ViaVarejoOrder,
            "bank_viavarejo": ViaVarejoBankBoleto,
            "bank_pago_detalhado_viavarejo": ViaVarejoBankCard,
            "order_integra_magazine": MagazineOrder,
            "bank_magazine": MagazineBank,
            "order_leroy": CarrefourOrder,
            "transaction_leroy": LeroyBank,
            "order_b2w": B2wOrder,
            "bank_repasse_b2w": B2wBank,
            "order_alpe": AlpeOrder,
            "bank_alpe": AlpeBank,
            "order_amazon": AmazonOrder,
            "bank_amazon": AmazonBank,
            "order_netshoes": NetshoesOrder,
            "bank_netshoes": NetshoesBank,
            "bank_dafiti": DafitiBank,
            "order_mm": MmOrder,
            "bank_mm": MmBank,
            "bank_zoom": ZoomBank,
            "order_gpa": GpaOrder,
            "bank_gpa": GpaBank
        }

        if signature_shelf.get(self.process):
            return self.build(signature_shelf.get(self.process))

    def build(self, class_signature):
        product = class_signature(self.file_path, self.file_content)

        if hasattr(product, 'empty'):
            if product.empty:
                product._setFileStatus('Vazio')
                return False, False
                
        return product.readFile()
