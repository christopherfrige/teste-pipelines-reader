from utilities.SlackMenssager import SlackMenssager
from services.TrackcashServices import TrackcashServices


class PrematureFailure:
    def __init__(self):
        self.trackcashServices = TrackcashServices()
        self.slackMenssager = SlackMenssager()

    def commitError(self,message):
        self.trackcashServices.sendErrors(message)
        self.slackMenssager.send(message)

    def bucketError(self,bucket,file):
        error = {"code":1,"message":f"Access problem/unavailable file\nBucket:{bucket}\nFile:{file}"}
        self.commitError(error)

    def unavailableEntitie(self,file):
        error = {"code":2,"message":f"Entitie class unavailable for this processs\nFile:{file}"}
        self.commitError(error)

    def unavailableToken(self,token,typeToken):
        error = {"code":3,"message":f"Unavailable token\nToken:{token}\nType token:{typeToken}"}
        self.commitError(error)

    def requestError(self,url,statusCode):
        error = {"code":4,"message":f"Error on request\nURL:{url}\nStatus:{statusCode}"}
        self.commitError(error)

    def readingError(self,file,bucket,error):
        error = {"code":5,"message":f"Error on read file\nFile:{file}\nBucket:{bucket}\nError:{error}"}
        self.commitError(error)

