import requests
import json
from utilities.GlobalVariables import HEADER_TRACKCASH, URL_API_CONFIGS, URL_API_BALANCES, URL_API_CHANNELS


class TrackcashRefferences:
    def __init__(self, idStore, code):
        self.idStore = idStore
        self.code = code

        self.idChannel = self.getIdChannel()
        self.balance = self.getBalance()
        self.configId = self.getConfigId()

    def _send_request(self, url, params=None):

        result = requests.get(
            url, params, headers=HEADER_TRACKCASH
        )

        if result.status_code == 200:
            return json.loads(result.content)
        else:
            print(f"Records not found in {url}, status code: {result.status_code}")
            return False

    def getIdChannel(self):

        results = self._send_request(URL_API_CHANNELS)

        if results:
            results = results.get('data')
            newResults = {}

            for item in results:
                newResults.update({item.get('code'): item.get('id')})

            return newResults


    def getBalance(self):

        results = self._send_request(URL_API_BALANCES)
        if results:
            parsed_dict = {}

            for item in results:
                parsed_dict.update({item.get('id'): item.get('description')})

            return parsed_dict

    def getConfigId(self):

        params = {
            "id_store": self.idStore,
            "code": self.code,
            "key": 'status_'+self.code
        }

        results = self._send_request(URL_API_CONFIGS, params)

        if results:
            try:
                configs = results.get('data')[0].get('value').get('Config')

                if configs == []:
                    configs = {}

                return configs
            except Exception as e:
                return {}
