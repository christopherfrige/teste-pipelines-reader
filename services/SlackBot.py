from utilities.GlobalVariables import SLACK_TOKEN
from utilities.EnviromentVariables import ENVIRONMENT_TYPE
from slack_sdk.web.client import WebClient


class SendErrorMessage:
    def __init__(self, msg=None):
        self.token = SLACK_TOKEN
        self.channel = "#reader" if ENVIRONMENT_TYPE == "PROD" else "#reader-dev"
        self.msg = msg
        self.client = None

    def start(self):
        if self.token:
            self.client = WebClient(token=self.token)
            if not self.check_dic():
                self.send_error_message()
                return True
            else:
                raise Exception("IncorrectKeyValue")

    def check_dic(self):
        if self.msg.get('id_store') is None or self.msg.get('id_store') is None or self.msg.get('id_store') is None or self.msg.get('id_store') is None:
            return True  # Validate Error
        else:
            return False

    def send_error_message(self):
        self.client.chat_postMessage(channel=self.channel,
                                     as_user=True,
                                     text=f"Erro na loja {self.msg.get('id_store')}",
                                     blocks=[
                                         {
                                             "type": "header",
                                             "text": {
                                                 "type": "plain_text",
                                                 "text": f"Problema com a loja {self.msg.get('id_store')}"
                                             }
                                         },
                                         {
                                             "type": "section",
                                             "text": {
                                                 "type": "mrkdwn",
                                                 "text": f"- Canal: {self.msg.get('channel')} \n - Arquivo: {self.msg.get('path')}"
                                             }
                                         },
                                         {
                                             "type": "section",
                                             "text": {
                                                 "type": "mrkdwn",
                                                 "text": f"*Erro*: {self.msg.get('error')}"
                                             }
                                         },
                                         {
                                             "type": "divider"
                                         }
                                     ]

        )
        return True


if __name__ == "__main__":
    msg = {
        "id_store": '1',
        "channel": '2',
        "path": 'teste/testando/test',
        "error": 'Teste slack bot',
    }
    slack = SendErrorMessage(msg)
    slack.start()