import boto3
import io


class Bucket:

    def __init__(self, name):
        self.s3 = boto3.resource('s3')
        self.name = name

    def get_file(self, file_name):
        return io.BytesIO(self.s3.Object(bucket_name=self.name, key=file_name).get()['Body'].read())
