import calendar
from datetime import datetime, timedelta

class DateManipulator:

    #Manipulador de data para repasse Leroy
    def leroyCicleCase(self,lastDate):

        if lastDate.day == 10:
             return (lastDate + timedelta(days=10)).strftime('%Y-%m-%d - %H:%M:%S')

        else:
            newMonth = lastDate.month + 1
            year = lastDate.year

            if newMonth > 12:
                newMonth = newMonth - 12
                year += 1

                paymentDate = datetime(year, newMonth, 5)

                paymentDateStringFormat = datetime.strftime(
                    paymentDate, 
                    "%Y-%m-%d - %H:%M:%S"
                )

                return paymentDateStringFormat

            else:

                paymentDate = datetime(year, newMonth, 5)

                paymentDateStringFormat = datetime.strftime(
                    paymentDate, 
                    "%Y-%m-%d - %H:%M:%S"
                )

                return paymentDateStringFormat

    #Manipulador de data para repasse Leroy
    def carrefourCicleCase(self,lastDate):

        if lastDate.day == 11:
             return (lastDate + timedelta(days=9)).strftime('%Y-%m-%d - %H:%M:%S')

        else:
            newMonth = lastDate.month + 1
            year = lastDate.year

            if newMonth > 12:
                newMonth = newMonth - 12
                year += 1

                paymentDate = datetime(year, newMonth, 10)

                paymentDateStringFormat = datetime.strftime(
                    paymentDate, 
                    "%Y-%m-%d - %H:%M:%S"
                )

                return paymentDateStringFormat

            else:

                paymentDate = datetime(year, newMonth, 10)

                paymentDateStringFormat = datetime.strftime(
                    paymentDate, 
                    "%Y-%m-%d - %H:%M:%S"
                )

                return paymentDateStringFormat

    @staticmethod
    def convert_date(date, enter_format, exit_format):
        return datetime.strptime(
            date,
            enter_format
        ).strftime(
            exit_format
        )

    def repasse_date_to_netshoes(self, date, enter_format):
        date_datetime = datetime.strptime(
            date,
            enter_format
        )

        if 5 < date_datetime.day <= 15:
            date_datetime = self.add_one_month(date_datetime)
            date_datetime = date_datetime.replace(day=5)
        
        else:
            date_datetime = date_datetime.replace(day=20)

        return date_datetime.strftime('%Y-%m-%d - %H:%M:%S')

    @staticmethod
    def add_one_month(orig_date):
        # advance year and month by one month
        new_year = orig_date.year
        new_month = orig_date.month + 1
        # note: in datetime.date, months go from 1 to 12
        if new_month > 12:
            new_year += 1
            new_month -= 12

        last_day_of_month = calendar.monthrange(new_year, new_month)[1]
        new_day = min(orig_date.day, last_day_of_month)

        return orig_date.replace(year=new_year, month=new_month, day=new_day)