from utilities.EnviromentVariables import TOKEN

URL_API_ORDERS_PROD = 'https://supervisord.trackcash.com.br/api/mkp/orders'

URL_API_REPASSE_PROD = 'https://supervisord.trackcash.com.br/api/mkp/payments'

URL_API_ORDERS_TESTE = 'https://test.trackcash.com.br/api/mkp/orders'

URL_API_REPASSE_TESTE = 'https://test.trackcash.com.br/api/mkp/payments'

URL_API_CONFIGS = 'https://sistema.trackcash.com.br/api/configs'

URL_API_CHANNELS = 'https://sistema.trackcash.com.br/api/channel'

URL_API_STATUS = 'https://sistema.trackcash.com.br/api/status'

URL_API_BALANCES = 'https://sistema.trackcash.com.br/api/balances/code'

URL_API_ERRORS = 'https://sistema.trackcash.com.br/api/qa/bots'

URL_API_STATUS_FILE_TEST = 'https://test.trackcash.com.br/api/archive'

URL_API_STATUS_FILE_PROD = 'https://sistema.trackcash.com.br/api/archive'

SLACK_TOKEN = 'xoxb-661208909446-1792978261607-fdbxm9KjbQJiu0IS4W5icBjK'

HEADER_TRACKCASH = {
    "Content-type":'application/json',
    'token':TOKEN,
    #'token':'amVzc2ljYS5kaWFzQHRyYWNrY2FzaC5jb20uYnI6amVqZTExMTI=',
    'User-Agent': 'XY'
}