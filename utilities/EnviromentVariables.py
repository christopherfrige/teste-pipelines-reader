import os

TOKEN = os.getenv('TOKEN')
ENVIRONMENT_TYPE = os.getenv('ENV_TYPE')
TOKEN_SLACK = os.getenv('TOKEN_SLACK')
