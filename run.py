import time
import urllib
from services.Factory import Factory
from utilities.EnviromentVariables import ENVIRONMENT_TYPE

def main(_event, context):

    # TODO: Implementar um classe para gerenciamento dos erros

    timeStart = time.time()

    path = urllib.parse.unquote_plus(_event.get('Records')[0].get('s3').get('object').get('key'))
    bucket = _event.get('Records')[0].get('s3').get('bucket').get('name')

    print(path)

    pathKey = path.split('/')
    process = pathKey[4]

    factory = Factory(path,bucket,process)

    code, response = factory.start()

    totalTime = (time.time() - timeStart)

    response = {"api": code, "Environment Type": "DEV" if not ENVIRONMENT_TYPE else ENVIRONMENT_TYPE, "Processing Time": totalTime}
    print(response)

    return response

if __name__ == '__main__':
    # event = {
    #     "bucket":'testes-leitor',
    #     "key":'integracao\\storage\\app\\archives\\stores\\25\\carrefour\\transaction_carrefour\\1\\20202810\\MKTPLACE_REPASSE_DETALHADO_318-20200811-20200825 (1).xlsx'
    # }

    # event = {
    #     "bucket":'testes-leitor',
    #     "key":"integracao\\storage\\app\\archives\\stores\\912\\cnova\\order_cnova\\1\\20201412\\pedidos_cnova.xlsx"
    # }

    # event = {
    #     "bucket":'testes-leitor',
    #     "key":'integracao\\storage\\app\\archives\\stores\\912\\cnova\\bank_cnova\\1\\20201012\\bank_cnova.csv'
    # }

    # event = {
    #     "Records":[
    #         {
    #             "s3":{
    #                 "bucket":{
    #                     "name":'testes-leitor'
    #                 },
    #                 "object":{
    #                     "key":"teste\\archives\\stores\\611\\magazine\\order_integra_magazine\\1\\20201221\\order_magazine.csv"
    #                 }
    #             }
    #         }
    #     ]
    # }4

    event = {
        "Records": [
            {
                "s3": {
                    "bucket": {
                        "name": "trackcash-file-channel-input"
                    },
                    "object": {
                        "key": 'archives/stores/1280/cnova/bank_cnova/1/2021-05-15/bank_via_254751337_2021-05-15.csv.reader'
                        # uma linha 'archives/stores/611/b2w/order_b2w/1/2021-05-16/order_b2w_2636692_01a388c4-aa07-4090-89d7-099d38a46c6c.csv.reader'
                        # 'archives/stores/1370/cnova/bank_cnova/1/2021-05-10/bank_via_263811334_2021-05-10.csv.reader'
                        # 'archives/stores/611/b2w/bank_repasse_b2w/1/2021-05-04/bank_repasse_b2w_ReportCC58731662000111B2W_PARCEIRO_5C88ED36A6EB4FE2B529357340.csv.reader'
                        # "archives/stores/611/cnova/bank_pago_detalhado_cnova/1/2021-05-14/bank_card_via_2021-05-14.csv.reader"
                        # b2w tst order -"archives/stores/1371/b2w/order_b2w/1/2021-05-16/order_b2w_2636828_1f41969e-550a-42f4-ad11-294b7f85f6a3.csv.reader"
                        # 'archives/stores/1189/cnova/bank_cnova/1/2021-05-14/bank_via_232355964_2021-05-14.csv.reader'
                        # 'archives/stores/1260/cnova/bank_cnova/1/2021-05-14/bank_via_178321336_2021-05-14.csv.reader'
                        # 'archives/stores/1349/cnova/bank_cnova/1/2021-05-14/bank_via_227294784_2021-05-14.csv.reader'
                        # 'archives/stores/611/cnova/bank_cnova/1/2021-05-14/bank_via_254741334_2021-05-14.csv.reader'
                        # "archives/stores/405/cnova/bank_cnova/1/2021-05-14/bank_via_175171335_2021-05-14.csv.reader"
                        # 'archives/stores/1260/cnova/bank_cnova/1/2021-05-14/bank_via_178321336_2021-05-14.csv.reader'
                        #'archives/stores/1375/b2w/order_b2w/1/2021-05-14/order_b2w_2635540_ea6f18c8-3c7c-48c8-a3fe-6b20b12f9b6f.csv.reader'
                        # "archives/stores/1370/b2w/order_b2w/1/2021-05-14/order_b2w_2635539.csv.reader"
                    }
                } 
            }
        ]
    }

    main(event, None)
