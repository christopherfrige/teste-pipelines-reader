from services.SlackBot import SendErrorMessage
from unittest import TestCase
import unittest


class TestSlackBot(TestCase):

    def setUp(self) -> None:
        self.fake_dic = {
            "id_store": "574",
            "chanel": "magazine",
            "caminho": "archives/stores/574/magazine/bank_magazine/1/20210304/bank_magazine_2.xlsx.reader",
            "errors": "id_withdrawal_teste_teste"
        }

        self.slack_bot = SendErrorMessage(self.fake_dic)

    def test_get_msg(self):

        test_msg = self.slack_bot.start()

        self.assertTrue(test_msg)


    def test_fake_msg(self):
        with self.assertRaises(Exception) as context:
            self.slack_bot.start()
        self.assertTrue('IncorrectKeyValue' in context.exception)



if __name__ == '__main__':
    unittest.main()
