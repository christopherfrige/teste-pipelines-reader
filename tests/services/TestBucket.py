from services.Bucket import Bucket
from unittest import TestCase
import io

class TestBucket(TestCase):

    def setUp(self) -> None:
        self.bucket = Bucket('chris-testes-leitor')

    def test_get_file(self):
        file_bytes = self.bucket.get_file('archives/stores/2/zoom/bank_zoom/1/20210101/25-01-2021.xls.reader')

        self.assertIsInstance(file_bytes,io.BytesIO)

    def test_fake_file(self):

        with self.assertRaises(Exception) as context:
            self.bucket.get_file('archives/stores/2/zoom/bank_zoom/1/20210101/fake-file')

            self.assertTrue('File not found!' in context.exception)

