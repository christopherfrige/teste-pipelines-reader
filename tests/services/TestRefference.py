import unittest
from unittest.mock import patch
from services.TrackcashRefferences import TrackcashRefferences
from utilities.GlobalVariables import HEADER_TRACKCASH, URL_API_CONFIGS, URL_API_BALANCES, URL_API_CHANNELS


class TestRefference(unittest.TestCase):

    def setUp(self) -> None:
        self.reference = TrackcashRefferences(2, 'netshoes')

    def test_send_request(self):

        with patch('services.TrackcashRefferences.requests') as mocked_response:
            mocked_response.return_value.status = 401
            mocked_response.return_value.content = b'{"response":"api_token Inv\xc3\xa1lido"}'

            self.assertEqual(
                mocked_response.return_value.status,
                401
            )


if __name__ == "__main__":
    unittest.main()
