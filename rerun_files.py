# Código para rodar todos os arquivos das loja do arr id_store_list
# para todas as datas dentro do arr de dates, o código irá iniciar
# um cliente do S3 e verificar todos as pastas dento do escopo do canal
# e do processo que se esta em busca.
# Obs.: É necessário se ter cuidado com o account

import boto3

from run import main

id_store_list = ('1260', '405', '1353', '1042', '1339', '1254', '1351', '541', '742', '1164', '895', '1255', '1189', '1090', '496', '1071', '1175', '1330', '849', '1129', '867', '1280', '859', '1333', '1272', '743', '1343', '1262', '1101', '1338', '611', '155', '2', '1286', '785', '1195', '1313', '1341', '1370', '1349', '912', '476')
dates = ('2021-05-14', '2021-05-15','2021-05-16','2021-05-17','2021-05-18','2021-05-19','2021-05-20','2021-05-21','2021-05-22','2021-05-23','2021-05-24','2021-05-25','2021-05-26','2021-05-27')
channel = 'cnova'
process = 'order_cnova'



s3 = boto3.client("s3")

files = list()

for item in id_store_list:
    for date in dates:
        all_objects = s3.list_objects_v2(
            Bucket = 'trackcash-file-channel-input',
            Prefix = f'archives/stores/{item}/{channel}/{process}/1/{date}/'
        )

        if not all_objects.get('Contents'):
            continue

        for data in all_objects.get('Contents'):
            files.append(data.get('Key'))


# print(all_objects)
for file in files:
    event = {
        "Records": [
            {
                "s3": {
                    "bucket": {
                        "name": "trackcash-file-channel-input"
                    },
                    "object": {
                        "key": file
                    }
                } 
            }
        ]
    }

    main(event, None)